\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Definition SAT-Problem}{2}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Exkurs Komplexit\IeC {\"a}tstheorie}{2}{subsection.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.1}Die Klasse P}{2}{subsubsection.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.2}Die Klasse NP}{2}{subsubsection.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.3}Das P-NP-Problem}{2}{subsubsection.10}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.4}NP-vollst\IeC {\"a}ndig}{2}{subsubsection.11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}NP-Vollst\IeC {\"a}ndigkeit des SAT-Problems}{3}{subsection.13}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}L\IeC {\"o}sungsstrategien}{3}{section.16}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Serielle Algorithmen}{3}{subsection.17}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1}Vollst\IeC {\"a}ndige Algorithmen}{3}{subsubsection.18}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.2}Unvollst\IeC {\"a}ndige Algorithmen}{4}{subsubsection.21}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Parallele Algorithmen}{4}{subsection.22}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}SAT-Competition}{4}{section.25}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Evaluierte Qualit\IeC {\"a}ten von SAT-Solvern}{4}{subsection.26}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.1}Main Track}{4}{subsubsection.27}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.2}Parallel Track}{4}{subsubsection.28}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.3}Incremental Library Track}{5}{subsubsection.29}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.4}Cloud Track}{5}{subsubsection.30}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Diskussion und Ausblick}{5}{section.31}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Fazit}{5}{section.40}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Abk\IeC {\"u}rzungen}{6}{section*.41}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Literatur}{6}{section*.41}
