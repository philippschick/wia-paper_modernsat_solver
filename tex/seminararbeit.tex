\documentclass[conference,compsoc,final,a4paper]{IEEEtran}
\usepackage[utf8]{inputenx}

%% Bitte legen Sie hier den Titel und den Autor der Arbeit fest
\newcommand{\autoren}[0]{Schick, Philipp}
\newcommand{\dokumententitel}[0]{Moderne SAT-Solver: Eine kurze Einführung}

\input{preambel} % Weitere Einstellungen aus einer anderen Datei lesen

\begin{document}

% Titel des Dokuments
\title{\dokumententitel}

% Namen der Autoren
\author{
  \IEEEauthorblockN{\autoren}
  \IEEEauthorblockA{
    Hochschule Mannheim\\
    Fakultät für Informatik\\
    Paul-Wittsack-Str. 10,
    68163 Mannheim
    }
}

% Titel erzeugen
\maketitle
\thispagestyle{plain}
\pagestyle{plain}

% Eigentliches Dokument beginnt hier
% ----------------------------------------------------------------------------------------------------------

% Kurze Zusammenfassung des Dokuments
\begin{abstract}
Das SAT-Problem gehört zu den wichtigsten und interessantesten Problemen der Theoretischen Informatik. Eine Lösung hierfür zu finden ist für zahlreiche praktische Probleme interessant; doch es ist auch das erste Problem, für das bewiesen wurde, dass es in die Menge der NP-Schweren Probleme fällt und sich deshalb nicht effizient lösen lässt. Um trotzdem in der Praxis möglichst effizient SAT-Probleme lösen zu können, wird bereits lange an sogenannten SAT-Solvern gearbeitet. Mit dem Voranschreiten der Entwicklung im Bereich der Multiprozessor-Systeme gewinnt die Entwicklung neuer SAT-Solver, welche diese neuen Prozessoren ausnutzen können, an Bedeutung. Um die Forschung in diesem Bereich voranzutreiben, findet jedes Jahr der SAT-Competition statt, der den schnellsten unter den eingereichten SAT-Solvern küren soll.
\end{abstract}

% Inhaltsverzeichnis erzeugen
{\small\tableofcontents}

% Abschnitte mit \section, Unterabschnitte mit \subsection und
% Unterunterabschnitte mit \subsubsection
% -------------------------------------------------------
\section{Einleitung}
Das Erfüllbarkeitsproblem der Aussagenlogik gehört zu den wichtigsten Problemen der Theoretischen Informatik. Im Allgemeinen wird es mit seinem englischen Namen \ac{SAT} bezeichnet \autocite[397]{Vossen2016}. Bei diesem Problem geht es um die Frage, ob es für eine Aussagenlogische Formel eine Belegung gibt, so dass die Formel zu \enquote{wahr} ausgewertet wird.
Das \ac{SAT}-Problem ist das erste Problem, für das gezeigt wurde, dass es in die Komplexitätsklasse der sogenannten NP-vollständigen Probleme fällt \autocite{Cook1971}.
NP-Vollständige Probleme lassen sich nicht in polynomieller Zeit mit einer deterministischen Turingmaschine lösen.
Somit  benötigt die exakte Lösung von \ac{SAT}-Problemen häufig mehr Rechenzeit als für eine praktische Anwendung zur Verfügung steht. Um trotzdem für praktische Probleme eine Lösung in akzeptabler Zeit zu finden, nutzt man sogenannte SAT-Solver, also Algorithmen und Programme, welche eine Lösung für eine Instanz des \ac{SAT}-Problems finden \autocite{Schoening2010}.\\
Anwendung findet das \ac{SAT}-Problem zum Beispiel  bei der formalen Verifikation von Hardware im Rahmen von automatisiertem Design von Elektronik \autocite[457]{Biere2009}. Auch um andere NP-Vollständige Probleme zu lösen, kommen \ac{SAT}-Solver zum Einsatz. Probleme aus vielen verschiedenen Anwendungsbereichen wie das Problem des Handlungsreisenden oder das Stundenplanproblem, können gelöst werden indem man sie als eine Aussagenlogische Formel formuliert, welche genau dann \enquote{wahr} ist, wenn das Problem gelöst ist. Diese Formel lässt man dann durch einen \ac{SAT}-Solver lösen \autocite{Schoening2010}.\newline \newline
Dieses Paper gibt eine Einführung in das Erfüllbarkeitsproblem der Aussagenlogik und stellt den SAT-Competition vor, welcher einen wichtigen Beitrag zur Entwicklung neuer SAT-Solver leistet.\newline
In Abschnitt \ref{definition} wird zunächst das \ac{SAT}-Problem definiert und in einem Exkurs zur Komplexitätstheorie verdeutlicht, warum eine Lösung des Problems so schwierig und doch so interessant für die Informatik ist. Im Abschnitt \ref{strategien} werden die verschiedenen Arten von SAT-Solvern eingeführt. Der \ref{competition}. Abschnitt stellt den SAT-Competition vor, welcher einen wichtigen Beitrag zu Forschung an SAT-Solvern leistet. Im \ref{ausblick}. und damit vorletzten Abschnitt werden einige Herausforderungen vorgestellt, der sich die Forschung im Bereich der SAT-Solver stellen muss. Im letzten Abschnitt werden die Inhalte des Papers zusammengefasst.

% -------------------------------------------------------

\section{Definition SAT-Problem} \label{definition}
Um das \ac{SAT}-Problem zu definieren, benötigt man zunächst die Definition für eine Boolsche Algebra. Dazu benötigt man eine Menge $B=\{0,1\}$. Auf dieser Menge sind Operatoren für die Konjunktion ($\wedge$), die Disjunktion ($\vee$) und die Negation definiert. Außerdem gilt für die Operatoren auf der Menge $B$ das Kommutativ-, Assoziativ- und das Distributivgesetz sowie die Existenz eines neutralen Elements und der Inversen für jedes Element \autocite[192]{Beutelspacher2007}.\\
Mit dieser Boolschen Algebra kann man nun die Menge aller Aussagenlogischen Formeln bilden.
\begin{align*}
WFF:=\textrm{Menge aller Aussagenlogischenformeln}
\end{align*}
Jeder Aussagenlogischen Formel $x \in WFF$ kann eine Belegung $\sigma$ zugeordnet werden. Dazu muss man jeder Variable von $x$ einen Wert 0 (falsch) oder 1 (wahr) zu weisen. Eine solche Kombination aus Formel und Belegung kann man jetzt auswerten und damit auf wiederum auf die Menge $\{0,1\}$ abbilden.\\
Eine Aussagenlogische Formel $x$ heißt nun \textit{erfüllbar (satisfiable)}, wenn es eine Belegung $\sigma$ gibt, sodass $x$ auf 1 abgebildet wird.
Damit können wir nun die Menge aller erfüllbaren aussagenlogischen Formeln definieren als:
\begin{align*}
SAT = \{x \in WFF \mid x \textrm{ ist erfüllbar}\}
\end{align*}
Zusammenfassend kann man also formulieren, dass das \ac{SAT}-Problem die Frage darstellt, ob zu einer gegebenen Aussagenlogischen Formel $x$ eine Belegung $\sigma$ existiert, so dass $x$ erfüllbar ist \autocite[398]{Vossen2016}.

\subsection{Exkurs Komplexitätstheorie}
Einleitend wurde bereits erwähnt, dass es sich bei dem \ac{SAT}-Problem um ein Problem handelt, welches im Allgemeinen schwierig zu lösen ist. Um den Grund hierfür zu verstehen, muss man wissen was eine Turingmaschine ist und wo der Unterschied zwischen einer deterministischen und einer nichtdeterministischen Turingmaschine liegt.\\
Eine Turingmaschine ist ein theoretisches Modell, mit dem ein Algorithmus formalisiert werden kann \autocite[112]{Hromkovic2014a}. Eine Turingmaschine besteht aus:
\begin{enumerate}
\item einem endlichen Zustandsautomaten, welcher das Programm enthält
\item mindestens einem Band (es können auch mehrere sein), welches als Eingabe und Speicher dient
\item einem Schreib-/Lesekopf, welcher sich vorwärts und rückwärts auf dem Band bewegen kann
\end{enumerate}
Eine deterministische Turingmaschine liest bei jedem Arbeitsschritt die Stelle auf dem Band, an welcher sich der Lesekopf befindet und wechselt aufgrund ihres aktuellen Zustands und dem Inhalt des Bands in ihren nächsten Zustand. Auf das Band kann auch geschrieben werden, um Dinge zu speichern oder zu löschen. In jedem Arbeitsschritt bewegt sich der Lesekopf auf dem Band jeweils um einen Schritt nach links oder rechts oder hält seine Position \autocite[94]{Hromkovic2014a}. Eine Berechnung auf einer Turingmaschine gilt als beendet, wenn die Turingmaschine sich in einem akzeptierenden oder endgültig nicht akzeptierenden Zustand befindet \autocite[121]{Hromkovic2014a}.\\
Eine nichtdeterministische Turingmaschine ist dadurch gekennzeichnet, dass die Turingmaschine nicht zwingend in einen Folgezustand übergehen muss, sondern auch den aktuellen Zustand beibehalten kann. Eine einfache Abstraktion für nichtdeterministische Turingmaschinen ist, dass die Turingmaschine zuerst eine Lösung für das untersuchte Problem nichtdeterministisch \enquote{rät} und dann deterministisch überprüft, ob das \enquote{Geratene} korrekt ist \autocite[113]{Hromkovic2014a}.\\
Man kann Algorithmen, welche so als Turingmaschinen formalisiert wurden, in Abhängigkeit von der benötigten Zeit für ihre Ausführung, in verschiedene Komplexitätsklassen einteilen. Zwei dieser Komplexitätsklassen sind vor allem im Zusammenhang mit dem \ac{SAT}-Problem besonders interessant: die Klasse P und die Klasse NP. \\
Um diese Klassen zu definieren, müssen wir zunächst festlegen, was unter polynomieller Laufzeit zu verstehen ist. Algorithmen laufen in polynomieller Zeit, wenn es eine Implementierung für den Algorithmus gibt, so dass für seine Laufzeit $l$ gilt:
\begin{align*}
l \leq O(n^k) : k\geq2
\end{align*}
Dabei sei $n$ die Anzahl an Eingaben, welche dem Algorithmus übergeben werden \autocite[389]{Vossen2016}.
\subsubsection{Die Klasse P}
Die Klasse P ist definiert als die Menge aller Probleme, für welche es einen Algorithmus für einen deterministischen Turingautomaten gibt der das Problem in polynomieller Zeit löst \autocite[391]{Vossen2016}.
\subsubsection{Die Klasse NP}
Die Klasse NP dagegen ist definiert als die Menge aller Probleme, für welche es einen nichtdeterministischen Turingautomaten gibt, der das Problem in polynomieller Zeit löst \autocite[391]{Vossen2016}.\\
Diese beiden Definitionen unterscheiden sich augenscheinlich nur im Detail. Allerdings kann nach heutigem Stand der Technik keine nichtdeterministische Turingmaschine realisiert werden. Auch Probleme der Klasse NP in polynomieller Zeit auf einer deterministischen Turingmaschine auszuführen ist nach aktuellem Stand der Forschung nicht möglich.
\subsubsection{Das P-NP-Problem}
Wenn nun ein Weg gefunden wird, ein einzelnes Problem aus der Klasse NP, deterministisch in polynomieller Zeit zu lösen, dann kann damit gezeigt werden dass $NP \subseteq P$ ist. Der Beweis, dass $P\subseteq NP$ gilt, ist einfach zu erbringen, da man jede deterministische Turingmaschine einfach in eine nichtdeterministische umwandeln kann. Dafür konstruiert man eine nichtdeterministische Turingmaschine, die im deterministischen Teil der umzuwandelnden deterministischen Turingmaschine gleicht und deren nichtdeterministische Teil nicht genutzt wird \autocite[279]{Vossen2016}.\\
Wenn man also beide Teilmengen beweisen könnte, wäre dann $P=NP$ gezeigt.\newline Zu zeigen, ob $P=NP$ oder $P\neq NP$ ist, gilt als das ungelöste Problem der Theoretischen Informatik und wird \enquote{P-NP-Problem} genannt \autocite[394]{Vossen2016}.
\subsubsection{NP-vollständig}
Eine Untermenge von NP ist die Klasse der NP-vollständigen Algorithmen. Dabei handelt es sich um die Algorithmen, für die unter der Annahme $P\neq NP$ gilt, dass sie in NP, aber nicht in P, liegen (Vergleiche \autoref{komplexitaetsKlassen}) \autocite[395]{Vossen2016}. Das \ac{SAT}-Problem war das erste Problem, für das gezeigt wurde, dass es NP-Vollständig ist \autocite{Cook1971}.
\begin{figure}[!ht]
\centering
\includegraphics[width=8.5cm]{komplexitaetsKlassen}
\caption{Die Klassen P, NP und NP-Vollständig \autocite[396]{Vossen2016}}
\label{komplexitaetsKlassen}
\end{figure}

\subsection{NP-Vollständigkeit des SAT-Problems}
Um zu zeigen, dass ein Problem NP-Vollständig ist, müssen zwei Bedingungen erfüllt sein:
\begin{enumerate}
\item Das Problem liegt in NP
\item Jedes Problem aus NP lässt sich auf das untersuchte Problem polynomiell reduzieren
\end{enumerate}
Um zu zeigen, dass ein Problem die erste Bedingung erfüllt, konstruiert man eine nichtdeterministische Turingmaschine welche, das Problem im polynomieller Zeit löst.\\
Probleme, welche die zweite Bedingung erfüllen, bezeichnet man auch als NP-Schwere Probleme. Um zu zeigen, dass ein Problem NP-Schwer ist, muss man ein Problem aus NP mit Hilfe des betrachteten Problems lösen. Der Algorithmus, der das eine Problem in das Andere überführt, darf dabei auch nur polynomiel viel Zeit benötigen. Dieses Verfahren nennt sich polynomielle Reduktion \autocite[195]{Hromkovic2014}.\\
Will man nun für das \ac{SAT}-Problem zeigen, dass es NP-Vollständig ist, konstruiert man zuerst eine nichtdeterministische Turingmaschine, welche \ac{SAT} löst. Damit hat man gezeigt dass \ac{SAT} in NP liegt. Nun muss man zeigen, dass \ac{SAT} NP-Schwer ist \autocite{Cook1971}. Dafür konstruiert man aus einer nichtdeterministischen Turingmaschine, welche das \ac{SAT}-Problem löst, eine Boolsche Formel, die genau dann \enquote{wahr} ist, wenn die Turingmaschine eine Eingabe akzeptieren würde. Diese Formel wird nur polynomiell lang. Somit hat man eine polynomielle Reduktion durchgeführt und gezeigt, dass \ac{SAT} NP-Schwer ist \autocite{Cook1971}.

% --------------------------------------------------------------------

\section{Lösungsstrategien} \label{strategien}
Aufgrund seiner NP-Vollständigkeit, kann für das \ac{SAT}-Problem kein effizienter Algorithmus gefunden werden. Für viele praktische Anwendungen möchte man allerdings trotzdem \ac{SAT}-Probleme in möglichst kurzer Zeit lösen.\\
Systeme, die Varianten von \ac{SAT} lösen, nennt man SAT-Solver. Moderne SAT-Solver machen sich verschiedene Eigenschaften von praktischen \ac{SAT}-Problemen zu nutze und verwenden Features moderner Prozessoren, um in möglichst kurzer Zeit eine Lösung zu finden \autocite{Schoening2010}.

\subsection{Serielle Algorithmen}
Man kann SAT-Solver in zwei Klassen unterteilen: in \enquote{vollständige} und \enquote{unvollständige} Algorithmen. Lässt man einem \enquote{vollständigen} Algorithmus ausreichend Rechenzeit, dann liefert er immer ein Ergebnis, ob das betrachtete Problem \enquote{erfüllbar} oder \enquote{unerfüllbar} ist. Ein \enquote{unvollständiger} Solver dagegen verfolgt eine vollkommen andere Strategie. \enquote{Unvollständige} SAT-Solver gehen davon aus, dass die untersuchte Instanz von \ac{SAT} erfüllbar ist und suchen nur noch eine Belegung, welche die analysierte Formel erfüllt. Dass eine Formel erfüllbar ist, ergibt sich häufig aus dem Kontext, in welchem sie auftritt \autocite{Schoening2010}.

\subsubsection{Vollständige Algorithmen} \label{vollstaendigeAlgos}
Für viele Anwendungen ist es notwendig einen vollständigen SAT-Solver zu verwenden, da man nicht sagen kann, ob die untersuchte Formel überhaupt eine Lösung hat. Diese Gewissheit eine Lösung zu finden, hat zum Nachteil, dass diese SAT-Solver sehr ineffizient sind. Im Durchschnitt benötigen solche Algorithmen zwar nur polynomiell viel Zeit, aber im schlechtesten Fall kann die Laufzeit exponentiell werden \autocite{Gong2017}. Den meisten SAT-Solvern liegt heutzutage der \ac{DPLL} Algorithmus zugrunde. Er ist nach den Autoren der beiden Paper \autocite{Davis1960} und \autocite{Davis1962} benannt \autocite{Schoening2010}.\\
Voraussetzung für die Anwendung des Algorithmus ist, dass die Aussagenlogische Formel des Problems in der sogenannten \textit{konjunktiven Normalform} vorliegt. Das bedeutet die Formel besteht aus mittels \enquote{und} verknüpften kleineren Formeln, welche man auch \textit{Klauseln} nennt. In den Klauseln selbst sind alle Variablen und negierten Variablen mittels \enquote{oder} verknüpft. \autoref{konjuktiveForm} zeigt eine solche Formel in konjuktiver Normalform.
\begin{figure}[!ht]
\centering
$(a \vee c) \wedge (b \vee \overline{c} \vee d)$
\caption{Eine Aussagenlogische Formel in konjunktiver Normalform (selbst erstellt)}
\label{konjuktiveForm}
\end{figure}
\\Gibt es nun eine Klausel, welche nur eine Variable enthält, dann setzt der Algorithmus diese Variable zu 1 und entfernt damit die Negation der Variable aus allen Klauseln und entfernt die Klauseln, welche die Variable selbst enthalten. Daraufhin ruft der Algorithmus sich selbst wieder rekursiv auf. Gibt es keine Klausel, welche nur eine Variable enthält, dann wählt der Algorithmus eine Variable aus. Der Algorithmus setzt zuerst die ausgewählte Variable auf 1 und entfernt, wie eben bereits erwähnt, die betroffenen Klauseln und die Negationen der Variable. Danach ruft sich der Algorithmus erneut wieder selbst auf. Daraufhin führt er das Verfahren noch einmal für die Variable durch, setzt sie aber auf 0. Dadurch werden alle Klauseln, welche die Negation enthalten, entfernt und die Variable aus allen übrigen Klauseln gestrichen. Auch danach macht der Algorithmus einen Rekursionsschritt und ruft sich selbst auf.\\
Wenn der Algorithmus mit dieser Methode alle Klauseln entfernen konnte, beendet er und meldet, dass die untersuchte Formel lösbar ist. Stößt der Algorithmus auf eine leere Klausel, also eine Klausel aus der alle Variablen entfernt wurden, aber die Klausel selbst nicht, dann beendet er und meldet, dass die Formel unlösbar ist. Stellt der Algorithmus in einer Verzweigung fest, dass die Formel selbst zwar nicht, aber in einer anderen Verzweigung, lösbar ist. Dann wird ausgegeben, dass die Formel lösbar ist \autocite{Davis1960}.\newline
Dieser Algorithmus kann für die Praxis durch verschiedene Heuristiken noch weiter beschleunigt werden. Beispiele für solche Methoden sind Klausellernen, nichtchronologisches Backtracking und Restarts \autocite{Schoening2010}.

\subsubsection{Unvollständige Algorithmen}
Unvollständige SAT-Solver basieren meist auf lokaler Suche. Dabei ändert der Algorithmus die Belegung der untersuchten Formel minimal ab und untersucht, ob das zu einer Lösung geführt hat. Im einfachsten Fall wird immer nur ein Bit der Belegung pro Schritt geändert. Eine fortgeschrittenere Strategie ist es, das zu ändernde Bit so zu wählen, dass möglichst viele Klauseln nach der Änderung erfüllt sind. Wieder eine andere Strategie ist es, nur Variablen zu ändern, welche sich in einer noch unerfüllten Klausel befinden \autocite{Schoening2010}.\\
Eine Gefahr bei auf lokaler Suche basierenden Algorithmen sind sogenannte lokale Minima. Bei Anwendung dieser Strategien kann es aber zum Beispiel vorkommen, dass eine Belegung gefunden wird, bei der von 100 Klauseln nur einige wenige nicht erfüllt werden, aber eine weitere Änderung von einzelnen Variablen zu keinen besseren Ergebnis im Bezug auf die Anzahl der erfüllten Klauseln führt. Tritt dieser Fall ein spricht man davon, dass der Algorithmus in einem lokalen Minimum \enquote{feststeckt}. Gegen solche Probleme hilft es gelegentlich zufällige Änderungen zu machen, welche nicht der Optimierungsstrategie der lokalen Suche folgen \autocite{Schoening2010}.

\subsection{Parallele Algorithmen}
Durch die Optimierung der klassischen seriellen SAT-Solver lassen sich in der Praxis nur noch wenige Fortschritte im Bezug auf die Rechenzeit erzielen. Um SAT-Solver weiter zu beschleunigen, muss man die Möglichkeiten moderner Prozessoren ausnutzen und die Algorithmen für die parallele Ausführung sowie die Ausführung auf speziallisierten Prozessoren wie \ac{GPU} oder \ac{FPGA} optimieren \autocite{Gong2017}.\\
Der \ac{DPLL}-Algorithmus, welcher in \autoref{vollstaendigeAlgos} vorgestellt wurde, lässt sich bereits aufgrund seiner Struktur gut parallelisieren. Jedes Mal, wenn der Algorithmus wieder eine Variable auf 1 beziehungsweise auf 0 setzt, wird die daraus resultierende Formel an einen neuen Rechenknoten übergeben \autocite{Hoelldobler2011}. Eine Möglichkeit solche vollständigen Solver weiter zu beschleunigen sind sogenannte \textit{Portfolios}. Dabei werden mehrere SAT-Solver parallel gestartet, wobei jeder Heuristiken mit einer anderen Strategie nutzt. Weiter beschleunigt werden kann das noch indem sich die parallelen Solver ihr Wissen über Konfliktklauseln teilen, welche sie beim Klausellernen finden \autocite[15]{Balyo2018}.\\
Auch die unvollständigen SAT-Solver, welche auf lokaler Suche basieren, lassen sich mit mehreren Rechenkernen beschleunigen. Dabei gibt es zwei verschiedene Herangehensweisen. Die eine ist der bereits eben erwähnte Portfolio-Ansatz. Die zweite Herangehensweise sind sogenannte \enquote{Multiple Flips}. Dabei werden die Variablen der Formel in $k$ verschiedene Gruppen aufgeteilt. $k$ entspricht in der Regel der Anzahl an zu Verfügung stehenden Rechenkernen. Bei jedem Schritt wird nun pro Gruppe eine Variable geändert (\enquote{geflippt}). Das wird solange wiederholt bis eine Lösung für die Formel gefunden ist \autocite[23]{Balyo2018}.\\
Die Grundidee bei der Anwendung von Portfolios für die nicht vollständigen Algorithmen ändert sich nicht gegenüber den vollständigen SAT-Solvern. Der erste SAT-Solver, welcher auf diesem Verfahren basierte, war der gNovelty+ (v. 2)\autocite{Pham2009}. Er gewann 2009 den SAT-Competition im Parallel Track. Was bei der Portfolio-Methode nicht von den vollständigen SAT-Solvern übernommen werden kann, ist der Austausch von Konfliktklauseln. Es gab verschiedene Ansätze, Informationen zwischen den Gruppen eines Portfolios auszutauschen, aber bisher konnte noch keine Variante überzeugen \autocite[24]{Balyo2018}.

% --------------------------------------------------------------------

\section{SAT-Competition} \label{competition}
Der erste Wettbewerb mit dem Ziel, den besten SAT-Solver zu finden, fand 1992 in Paderborn statt. Er wurde von Michael Buro und Hans Kleine organisiert \autocite{Buro1992}. In den darauf folgenden Jahren entwickelten sich die SAT-Solver weiter und immer wieder veranstalteten verschiedene wissenschaftliche Einrichtungen Wettbewerbe, um den besten Solver zu finden. Im Jahr 2002 wurde dann der erste SAT-Competition ausgetragen, welcher die Reihe der seit dem jährlich veranstalteten Wettbewerbe begründete \autocite{Jaervisalo2012}. Diese Wettbewerbsreihe hat das Ziel, den aktuellen Stand der Forschung im Bezug auf SAT-Solving zu evaluieren und der Öffentlichkeit vorzustellen \autocite{Jaervisalo2012}.

\subsection{Evaluierte Qualitäten von SAT-Solvern}
Der SAT-Competition unterteilt sich in unterschiedliche Kategorien, sogenannte \textit{Tracks}. Jeder Track stellt andere Anforderungen an die SAT-Solver und nutzt andere Benchmarks um sie zu evaluieren. Hier sollen exemplarisch einige Tracks vorgestellt werden \autocite{Jarvisalo2021}.

\subsubsection{Main Track}
Das Ziel des Main Tracks ist, schnellstmöglich festzustellen, welche der gegebenen \ac{SAT} Instanzen in Konjuktiver Normalform lösbar ist. Der Wettbewerb dafür läuft in zwei Phasen ab. Nur die besten SAT-Solver der ersten Phase treten in der zweiten Phase an. Die Platzierung erfolgt an Hand der gelösten Probleme, wobei pro Problem ein Time-out zur Verfügung steht. Der Solver löst also solange Probleme bis er es nicht mehr schafft, ein Problem in der für das Problem verfügbaren Zeit zu lösen. Bei Gleichstand werden die kumulierten Zeiten aller Probleme verglichen. SAT-Solver, welche eine falsche Antwort auf ein Problem liefern werden, disqualifiziert \autocite{Jaervisalo2012}.

\subsubsection{Parallel Track}
Beim Parallel Track treten parallele SAT-Solver gegeneinander an. In der Regel basieren moderne parallele SAT-Solver auf einem Portfolio Ansatz. Dabei ist der Gewinner des Parallel Track aus dem Jahr 2011 hervorzuheben. Der PPfolio genannte Algorithmus lies die besten Algorithmen der letzten Jahre parallel laufen und gab das Ergebnis des ersten Algorithmus zurück, der eine Lösung gefunden hatte. Damit die Autoren der für einen solchen Ansatz genutzten Algorithmen nicht enttäuscht sind, weil ein anderer mit ihrem Algorithmus gewonnen hat, wurde in späteren Wettbewerben Regeln erlassen, welche solche Algorithmen in eine gesonderte Kategorie einordnet, ganz verbietet oder vorraussetzt, dass die Erlaubnis der Autoren der verwendeten Solver eingeholt wurde \autocite{Balyo2016}.

\subsubsection{Incremental Library Track} \label{IncLib}
Während für die Traditionellen Tracks die SAT-Solver über ein Commandline Interface gesteuert werden, müssen die SAT-Solver für den Incremental Library Track sich über eine vorgegebene API steuern lassen. Das hat den Zweck ein Standard Interface für SAT-Solver zu etablieren, so dass es Entwickler welche einen SAT-Solver in ihr System einbinden wollen es einfacher haben \autocite{Balyo2016}.

\subsubsection{Cloud Track}
Beim Cloud Track geht es darum einen SAT-Solver zu bauen, welcher möglichst schnell in einem verteilten System arbeitet. Das heißt der Algorithmus wird auf einem Cluster von 100, über das Ethernet verbundenen, Servern ausgeführt \autocite{Heule2020}.

% --------------------------------------------------------------------

\section{Diskussion und Ausblick} \label{ausblick}
Wie bei vielen Dingen in der Informatik, liegt auch die Zukunft des SAT-Solvings bei der Parallelverarbeitung. Zu der Zukunft der parallel arbeitenden SAT-Solver haben \citeauthor{Hamadi2012} im Jahr 2013 sieben Herausforderungen formuliert, die bereits teilweise von der Forschung angegangen wurden (siehe \autocite{Gong2017}), teils warten sie noch auf Lösungen.
\subsubsection*{Challenge 1}
Bei der ersten Herausforderung geht es darum, eine automatische Technik zu finden, um festzulegen, wie viele parallele Prozesse man für das vorliegende Problem am besten nutzt \autocite[2122]{Hamadi2012}. 

\subsubsection*{Challenge 2}
Die zweite Herausforderung ist einen Weg zu finden, die Untersuchten Formeln bzw. den untersuchten Lösungsraum so zu zerlegen, dass schneller als mit den bisherigen Methoden eine Lösung gefunden werden kann \autocite[2123]{Hamadi2012}.

\subsubsection*{Challenge 3}
Als dritte Herausforderung sehen \citeauthor{Hamadi2012}, es Techniken zu entwickeln, mit denen ein Problem so zerlegt werden kann, dass damit die Problemlösung erleichtert wird \autocite[2123]{Hamadi2012}.

\subsubsection*{Challenge 4}
In der vierten Herausforderung wird gefordert eine bessere Methode zu finden, um Klauseln zu bewerten, welche von einem anderen SAT-Solver beim Klausellernen gefunden wurden \autocite[2123]{Hamadi2012}.

\subsubsection*{Challenge 5}
Die Fünfte Herausforderung beschäftigt sich mit Primfaktorzerlegung. Dabei geht es darum für eine natürliche Zahl als ein Produkt aus Primzahlen darzustellen. Kann man dieses Problem effektiv lösen, können damit Angriffe auf die \ac{RSA} Verschlüsselung durchgeführt werden \autocite{Montgomery1994}. Die Herausforderung ist eine neue Codierung für Primfaktorzerlegungen zu finden, so dass sie sich besser mit einem SAT-Solver lösen lassen \autocite[2124]{Hamadi2012}.

\subsubsection*{Challenge 6}
Eine neue Codierung für SAT-Probleme zu finden, welche speziell für Parallele SAT-Solver geeignet ist, wird von \citeauthor{Hamadi2012} als sechste Herausforderung gesehen \autocite[2124]{Hamadi2012}.

\subsubsection*{Challenge 7}
Als siebte und letzte Herausforderung wurde formuliert, einen vollkommen neuen parallelen SAT-Solver zu konstruieren, welcher mindestens so gut ist, wie die Algorithmen, welche bisher auf kollisionsbasiertem Klausellernen aufbauen \autocite[2124]{Hamadi2012}.\newline

Die Autoren von \citetitle{Balyo2018} greifen diese Herausforderungen von \citeauthor*{Hamadi2012} auf und fügen noch drei Forderungen hinzu. Die Autoren äußerten die Meinung, dass \ac{SAT}-Solver entworfen werden sollten, welche auf großen Computer Clustern arbeiteten \autocite{Balyo2018}. Das wurde auch vom \ac{SAT}-Competition aufgegriffen und inzwischen gibt es mit dem Cloud Track eine eigene Kategorie in der Solver auf Verteilten Systemen in den Amazon Web Services ausgeführt werden \autocite{Heule2020}. Ein weiteres Ziel für die Zukunft müsse sein, \ac{SAT}-Solver zu entwickeln welche auf \ac{GPU}s ausgeführt werden, meinen \citeauthor{Balyo2018} \autocite{Balyo2018}. Auch an diesem Problem wird wie zum Beispiel in \autocite{Gong2017} dargelegt, bereits gearbeitet.\\
Viele SAT-Probleme ähneln einander und unterscheiden sich nur in wenigen Klauseln. Diese Eigenschaft machen sich bereits jetzt SAT-Solver zu nutzen, allerdings ist keiner dieser SAT-Solver bisher für Parallelverarbeitung ausgelegt. Deshalb wird die Entwicklung solcher \ac{SAT}-Solver eine Herausforderung für die Zukunft sein, meinen die Autoren \citeauthor{Balyo2018}. Wie bereits in \autoref{IncLib} erwähnt sind Inkrementelle \ac{SAT}-Solver ein Thema, mit dem sich bereits im \ac{SAT}-Competition beschäftigt wird. Allerdings liegt dabei der Fokus vorerst nicht darauf parallele \ac{SAT}-Solver zu entwickeln, sondern Solver für ein vorgegebenes und einheitliches Interface.

% --------------------------------------------------------------------

\section{Fazit} \label{fazit}
SAT-Solver sind eine Art von Algorithmen, welche die Informatik schon lange beschäftigen und vermutlich auch noch einige Zeit weiter beschäftigen werden.
Die Entwicklung von \ac{SAT}-Solvern ist noch lange nicht am Ende angelangt, noch immer gibt es Verbesserungspotential in den Algorithmen, was die Theoretische und die Angewandte Informatik weiter beschäftigen wird \autocite{Hamadi2012}.\\
Um neue und bessere Algorithmen zu finden, hat sich bereits in den letzten Jahren der \ac{SAT}-Competition hervorgetan \autocite{Balyo2016}. Er hat neue Lösungsansätze wie \textit{Portfolios} hervorgebracht \autocite{Balyo2018} und versucht neue Solver einfacher für die Industrie verfügbar zu machen mit dem standarisierten Interface für \ac{SAT}-Solver aus Kategorie Incremental Track \autocite{Balyo2016}. Eine weitere spannende Entwicklung in der Zukunft wird das Voranschreiten des Einsatzes von Cloudcomputing für das \ac{SAT}-Solving werden \autocite{Hamadi2012}. Auch dieser Entwicklung trägt der \ac{SAT}-Competition bereits  mit dem Cloud Track Rechnung \autocite{Heule2020}.
\newpage

% --------------------------------------------------------------------

\section*{Abkürzungen}
\addcontentsline{toc}{section}{Abkürzungen}

% Die längste Abkürzung wird in die eckigen Klammern
% bei \begin{acronym} geschrieben, um einen hässlichen
% Umbruch zu verhindern
% Sie müssen die Abkürzungen selbst alphabetisch sortieren!
\begin{acronym}[IEEE]
\acro{DPLL}{Davis Putnam Logemann Loveland}
\acro{FPGA}{Field Programmable Gate Array}
\acro{GPU}{Graphics Processing Units}
\acro{IF}{Integer Faktorisierung}
\acro{RSA}{Rivest–Shamir–Adleman}
\acro{SAT}{satisfiability problem}
\acro{WFF}{Well-Formed Formulas}
\end{acronym}

% Literaturverzeichnis
\addcontentsline{toc}{section}{Literatur}
\printbibliography

\end{document}
